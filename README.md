
Pragmatic debian packaging for unmangleOutlookSafelinks for Thunderbird
=======================================================================


See also
--------

https://addons.thunderbird.net/de/thunderbird/addon/unmangle-outlook-safelinks/


Building
--------

    $ dpkg-buildpackage


Obtaining a new upstream version
--------------------------------

    $ git remote add upstream https://github.com/phavekes/unmangleOutlookSafelinks
    $ git fetch upstream
    $ git checkout debian
    $ git merge … # version tag
